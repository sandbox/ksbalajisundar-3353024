Module to review the redirects added using the Redirect module and generate a report

DEPENDENCIES
This module depends on Core Views module to generate the report and 'Redirect' contributed module
- Views - Install
  - drush en views
- Redirect - Download and install
  - composer require drupal/redirect
  - drush en redirect

INSTALLATION
- Download the module through composer
 - composer require drupal/redirect_review
- Enable the module by going to /admin/modules or through drush
 - drush en redirect_review
