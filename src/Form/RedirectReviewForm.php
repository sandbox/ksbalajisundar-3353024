<?php
/**
 * @file
 * File to implement configuration form to initiate redirect review.
 */

namespace Drupal\redirect_review\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Class RedirectReviewForm provies a form to review redirects.
 */
class RedirectReviewForm extends ConfigFormBase {
  /**
   * Redirect config object
   *
   * @var mixed
   */
  protected $redirectReviewConfig;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->redirectReviewConfig = $config_factory->getEditable("redirect_review.redirect.review");
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * Get the form id.
   *
   * @return string
   *   Unique form id.
   */
  public function getFormId() {
    return 'redirect_review_redirect_review_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'redirect_review.redirect.review',
    ];
  }

  /**
   * Generate the Form.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   FormState Object.
   *
   * @return array
   *   Return Form Element.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['redirect_errors_intro'] = [
      '#type' => 'container',
      'intro' => [
        '#type' => 'markup',
        '#markup' => $this->t("
          This form is used to review the redirects and provide a report of redirects that have issues.
          This is done by performing a http request on the 'source' path using the domain specified here.
          This is run as a batch and it might take a while depending on the server capacity and the number of redirects.
          As it is resource expensive task, it is recommended not to run very frequently.
          Excludes webform and .pdf file paths by default.
          In future this will be added to the configuration.
          Do not add any trailing slashes."),
      ]
    ];

    // Get all redirects from the database.
    $all = $this->getAllRedirects();

    if (count($all)) {
      $form['redirect_review_load_domain'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Enter domain name to review redirects through http requests'),
        '#default_value' => $this->redirectReviewConfig->get('redirect_review_load_domain'),
        '#description' => $this->t('Enter domain name used in the redirects eg: https://www.domain.com or https://sub.domain.com etc. The script will look for redirect issues with this domain.'),
        '#maxlength' => 256,
        '#required' => TRUE
      ];

      $form['redirect_review_look_for_issues'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Look for redirect issues when saved'),
      ]; 

      $form['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Submit'),
        '#attributes' => ['class' => ['button', 'button--primary']],
      ];
    }
    else {
      $form['redirect_none'] = [
        '#type' => 'container',
        'none' => [
          '#type' => 'markup',
          '#markup' => "<br/><strong>" . $this->t("There are no redirects in this application to review.") . "</strong>",
        ]
      ];

      $form['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Submit'),
        '#disabled' => 'disabled',
        '#attributes' => ['class' => ['button', 'button--primary']],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save the domain in config.
    $this->redirectReviewConfig
      ->set('redirect_review_load_domain', $form_state->getValue('redirect_review_load_domain'))
      ->save();
  
    if ($form_state->getValue('redirect_review_look_for_issues')) {
      // Clear previous data.
      $this->redirectReviewConfig
      ->set('redirect_review_redirect_errors', '')
      ->save();

      // Get all redirects from the database.
      $all = $this->getAllRedirects();

      if (count($all)) {
        $batch = [
          'title' => $this->t('Validating redirect entries, this may take a while ...'),
          'operations' => [],
          'finished' => 'redirect_review_finished',
          'progress_message' => t('Processed @current out of @total.'),
          'error_message' => t('Batch has encountered an error.'),
        ];

        $default_lang = \Drupal::languageManager()->getDefaultLanguage()->getId();

        // Create a batch job for each redirect entry.
        foreach ($all as $entry) {
          $lang_code = $entry->lang;
          $lang = "/$lang_code/";

          // Handle languages.
          if ($lang_code == 'pt-br' || $lang_code == 'pt-pt') {
            $lang = 'pt';
          }
          elseif ($lang_code== 'und' || $lang_code == $default_lang) {
            $lang = "/";
          }

          // Exclude webform urls & .pdf files.
          // @todo - Add this to configuration.
          if (strpos($entry->source, "webform") !== FALSE || strpos($entry->source, ".pdf") !== FALSE) {
            continue;
          }

          // Process redirects in a batch.
          $batch['operations'][] = ['\Drupal\redirect_review\RedirectReviewLookup::lookForRedirectIssues', [
            $form_state->getValue('redirect_review_load_domain'),
            $lang,
            $entry->rid,
            $entry->source
          ]];
        }

        batch_set($batch);
      }
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * Function to all the redirects from the Redirect table.
   * 
   * @return Array
   *   An array of database values of redirects.
   */
  private function getAllRedirects() {
    $query = \Drupal::database()->select('redirect', 'rd');
    $query->addField('rd', 'rid', 'rid');
    $query->addField('rd', 'language', 'lang');
    $query->addField('rd', 'redirect_source__path', 'source');
    $all = $query->execute()->fetchAll();

    return $all;
  }
}
